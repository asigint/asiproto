from setuptools import setup

setup(name='AsiProto',
      version='1.0.0',
      description='Protobuf Messages for ASI Photon Interface',
      url='https://bitbucket.org/asigint/asiproto/',
      author='ASI',
      author_email='corporate@asigint.com',
      license='MIT',
      packages=['AsiProto'],
      install_requires=[
            'google',
            'protobuf'
      ],
      zip_safe=False)
