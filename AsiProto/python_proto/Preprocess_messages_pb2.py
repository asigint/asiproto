# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: Preprocess_messages.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import ASI_ID_pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='Preprocess_messages.proto',
  package='preprocessor',
  serialized_pb=_b('\n\x19Preprocess_messages.proto\x12\x0cpreprocessor\x1a\x0c\x41SI_ID.proto\"\x82\x01\n\x13PreprocessorRequest\x12\x1b\n\x13time_domain_enabled\x18\x01 \x01(\x08\x12 \n\x18\x66requency_domain_enabled\x18\x02 \x01(\x08\x12,\n\x06\x61si_id\x18\x05 \x01(\x0e\x32\x07.ASI_ID:\x13PreprocessorRequest')
  ,
  dependencies=[ASI_ID_pb2.DESCRIPTOR,])
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_PREPROCESSORREQUEST = _descriptor.Descriptor(
  name='PreprocessorRequest',
  full_name='preprocessor.PreprocessorRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='time_domain_enabled', full_name='preprocessor.PreprocessorRequest.time_domain_enabled', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='frequency_domain_enabled', full_name='preprocessor.PreprocessorRequest.frequency_domain_enabled', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='asi_id', full_name='preprocessor.PreprocessorRequest.asi_id', index=2,
      number=5, type=14, cpp_type=8, label=1,
      has_default_value=True, default_value=2001,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=58,
  serialized_end=188,
)

_PREPROCESSORREQUEST.fields_by_name['asi_id'].enum_type = ASI_ID_pb2._ASI_ID
DESCRIPTOR.message_types_by_name['PreprocessorRequest'] = _PREPROCESSORREQUEST

PreprocessorRequest = _reflection.GeneratedProtocolMessageType('PreprocessorRequest', (_message.Message,), dict(
  DESCRIPTOR = _PREPROCESSORREQUEST,
  __module__ = 'Preprocess_messages_pb2'
  # @@protoc_insertion_point(class_scope:preprocessor.PreprocessorRequest)
  ))
_sym_db.RegisterMessage(PreprocessorRequest)


# @@protoc_insertion_point(module_scope)
