# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ASI_ID.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ASI_ID.proto',
  package='',
  syntax='proto2',
  serialized_pb=_b('\n\x0c\x41SI_ID.proto*\x92\x01\n\x06\x41SI_ID\x12\x0c\n\x07\x43\x61lTone\x10\xd0\x0f\x12\x18\n\x13PreprocessorRequest\x10\xd1\x0f\x12\x11\n\x0c\x44\x66\x65TdRequest\x10\xd2\x0f\x12\x11\n\x0c\x44\x66\x65\x46\x64Request\x10\xd3\x0f\x12\x0f\n\nDfeResults\x10\xd4\x0f\x12\n\n\x05OTHER\x10\xd5\x0f\x12\x08\n\x03PNT\x10\xd6\x0f\x12\t\n\x04Omni\x10\xd7\x0f\x12\x08\n\x03RFE\x10\xd8\x0f')
)

_ASI_ID = _descriptor.EnumDescriptor(
  name='ASI_ID',
  full_name='ASI_ID',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='CalTone', index=0, number=2000,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PreprocessorRequest', index=1, number=2001,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfeTdRequest', index=2, number=2002,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfeFdRequest', index=3, number=2003,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DfeResults', index=4, number=2004,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='OTHER', index=5, number=2005,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='PNT', index=6, number=2006,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='Omni', index=7, number=2007,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RFE', index=8, number=2008,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=17,
  serialized_end=163,
)
_sym_db.RegisterEnumDescriptor(_ASI_ID)

ASI_ID = enum_type_wrapper.EnumTypeWrapper(_ASI_ID)
CalTone = 2000
PreprocessorRequest = 2001
DfeTdRequest = 2002
DfeFdRequest = 2003
DfeResults = 2004
OTHER = 2005
PNT = 2006
Omni = 2007
RFE = 2008


DESCRIPTOR.enum_types_by_name['ASI_ID'] = _ASI_ID
_sym_db.RegisterFileDescriptor(DESCRIPTOR)


# @@protoc_insertion_point(module_scope)
