#!/usr/bin/env bash
protoc -I='/opt/asi/devel/AsiProto/AsiProto' --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/Preprocess_messages.proto
protoc -I='/opt/asi/devel/AsiProto/AsiProto' --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/Dfe_messages.proto
protoc -I='/opt/asi/devel/AsiProto/AsiProto' --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/Frontend.proto
protoc -I='/opt/asi/devel/AsiProto/AsiProto' --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/ASI_ID.proto
protoc -I='/opt/asi/devel/AsiProto/AsiProto' --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/PNT_messages.proto
protoc -I='/opt/asi/devel/AsiProto/AsiProto' --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/DFMessages.proto