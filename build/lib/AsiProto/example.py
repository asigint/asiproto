from AsiProto.python_proto import Dfe_messages_pb2 as DFEMessage
from AsiProto.python_proto import Preprocess_messages_pb2 as PPMessage
from AsiProto.python_proto import Frontend_pb2 as FEMessage
import google.protobuf.json_format as json_format


if __name__ == "__main__":
    PreProcess_Request = PPMessage.PreprocessorRequest()
    PreProcess_Request.time_domain_enabled = True
    PreProcess_Request.frequency_domain_enabled = False
    PreProcess_Request.asi_id = PPMessage.ASI_ID_pb2.PreprocessorRequest

    DFE_TD_Request = DFEMessage.DfeTdRequest()
    DFE_TD_Request.asi_id = DFEMessage.ASI_ID_pb2.DfeTdRequest
    DFE_TD_Request.integration_time_in_seconds = 2.0

    Cal_Tone_Message = FEMessage.CalTone()
    Cal_Tone_Message.status = True
    Cal_Tone_Message.asi_id = FEMessage.ASI_ID_pb2.CalTone
    Cal_Tone_Message.calibration_complete = False

    DFE_Message = DFEMessage.DfeResults()

    ChanRes1 = DFEMessage.DfeResults.ChannelResult()
    ChanRes1.target = 3.0
    ChanRes1.aoa =  5.0
    ChanRes1.snr = 6.0
    ChanRes1.depression_angle = 59.0

    ChanRes2 = DFEMessage.DfeResults().ChannelResult()
    ChanRes2.target = 8.0
    ChanRes2.aoa = 107.5
    ChanRes2.snr = 89.4
    ChanRes2.depression_angle = 789.325

    DFE_Message.result.extend([ChanRes1])
    DFE_Message.result.extend([ChanRes2])
    DFE_Message.timestamp1 = 123456
    DFE_Message.timestamp2 = 678904
    DFE_Message.asi_id = DFEMessage.ASI_ID_pb2.DfeResults

    print('--- DFE Messages with Two Channel Result Messages ---')
    print(DFE_Message)

    print('--- PreProcess Request Message ---')
    print(PreProcess_Request)

    print('--- DF Time Domain Request Message ---')
    print(DFE_TD_Request)

    print('--- Cal Tone Message ---')
    print(Cal_Tone_Message)

    # Protobuf Message to JSON
    PreProcess_Request_json_string = json_format.MessageToJson(PreProcess_Request)
    print('--- PreProcess Request Messsage in JSON Format ---')
    print(PreProcess_Request_json_string)

    # JSON Message to Protobuf
    new_PPR_Message = json_format.Parse(PreProcess_Request_json_string, PPMessage.PreprocessorRequest())

    print('--- JSON to new Pre Processor Request Message Message ---')
    print(new_PPR_Message)

