## AsiProto
---
Repository for .proto files that define communication with Asi microservices in the photon framework.

## Instructions
---
### Protobuffer Installation
Follow the install instructions in Reference 1.

### ASI Installation
Assuming your environment is good to go:

1. Verify that protobuffer C++ Implementation is installed : $ protoc --version
2. Source virtual environment: $ source /opt/asi/devel/python_venv/bin/activate
3. Move into the AsiProto git directory : cd /opt/asi/devel/AsiProto
4. Install python module requirements : Run python setup.py install

### Compile python implementations
1. $ protoc -I='/opt/asi/devel/AsiProto/AsiProto'  --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/Preprocess_messages.proto
2. $ protoc -I='/opt/asi/devel/AsiProto/AsiProto'  --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/Dfe_messages.proto
3. $ protoc -I='/opt/asi/devel/AsiProto/AsiProto'  --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/Frontend.proto
4. $ protoc -I='/opt/asi/devel/AsiProto/AsiProto'  --python_out='/opt/asi/devel/AsiProto/AsiProto/python_proto/' /opt/asi/devel/AsiProto/AsiProto/ASI_ID.proto

## Files
---
1. AsiProto/Dfe_messages.proto - proto file that defines messages to DF Engine.
2. AsiProto/Frontend.proto - proto file that defines messages to RF Front End End
3. AsiProto/Preprocess_messages.proto - proto file that defines messages to Preprocesssor
4. AsiProto/ASI_ID.proto - proto file that defines message IDs for ASI protocol buffer messages
5. AsiProto/python_proto - contains python implementations of the above *.proto definitions
6. AsiProto/example.py - Example code for Protobuffer instantiation
7. setup.py - python setup script for installing package dependencies
---
## References
1. https://github.com/protocolbuffers/protobuf/tree/master/python
2. https://developers.google.com/protocol-buffers/docs/pythontutorial
3. https://developers.google.com/protocol-buffers/docs/reference/cpp-generated
---
## TODOs
1. Incorporate https://github.com/uber/prototool#quick-start to make compile process simpler
